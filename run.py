#! /usr/bin/env python3
import tkinter as tk
import tkinter.messagebox as messagebox
from tkinter import ttk

from mine import generate_minefield
from collections import deque


class Mine(tk.Frame):
    """Custom mine button

    This is implemented by subclassing a regular Frame and creating a label inside because
    regular button was hard to style (at least that's what I thought initially)
    """

    # Dictionary to get text color according to button value
    COLOR_MAP = {
        1: 'blue',
        2: 'darkgreen',
        3: 'red',
        4: 'navy',
        5: 'darkred',
        6: 'cyan4',
        7: 'black',
        8: 'gray42',
    }

    def __init__(self, *args, **kwargs):
        # Save value, will be used later
        self.value = kwargs['value']
        del kwargs['value']

        # Call default Frame constructor
        super().__init__(*args, **kwargs)

        # Need to set this to properly size the Frame
        self.pack_propagate(0)

        # Create the nested Label
        self._label = tk.Label(self)
        self._label.pack(fill=tk.BOTH, expand=1)

        # Default state is not visible and not flagged
        self._flag = False
        self._visible = False
        self._set_visible()  # Need to call this because we didn't set the values with setters

        # Configure style of Frame
        self.config(
            width=24,
            height=24,
            borderwidth=2,
            bg='gray',
            highlightbackground="gray42",
            highlightthickness=1,
        )

        # Configure style of nested Label
        self.label.config(
            bg='gray',
        )

        # Bind right click action
        self.bind('<Button-2>', self._handle_rightclick)

    def bind(self, sequence=None, func=None, add=None):
        """Override default bind method to bind simultaneously to Frame and Label"""
        super().bind(sequence, func, add)
        self.label.bind(sequence, func, add)

    # 'label' is a property to ensure read-only access from outside
    @property
    def label(self):
        return self._label

    # 'visible' and 'flag' are properties to ensure that _set_visible() is called
    # on value change.
    @property
    def visible(self):
        return self._visible

    @visible.setter
    def visible(self, value):
        self._visible = value
        self._set_visible()

    @property
    def flag(self):
        return self._flag

    @flag.setter
    def flag(self, value):
        self._flag = value
        self._set_visible()

    def _set_visible(self):
        """Updates button depending on visibility and flag state"""
        if self.visible:
            self.label.config(
                text='*' if self.value == -1 else str(self.value) if self.value > 0 else '',
                fg=self.COLOR_MAP.get(self.value, 'black'),
            )
            self.config(relief=tk.FLAT)
        else:
            self.label.config(text='' if not self.flag else '\u2691')
            self.config(relief=tk.RAISED)

    def _handle_rightclick(self, event):
        """Toggles flag if not already revealed"""
        if not self.visible:
            self.flag = not self.flag


class Minesweeper(tk.Tk):
    """Main window"""

    def __init__(self):
        super().__init__()

        # Smiley button images
        self._btn_alive = tk.PhotoImage(file='resources/smiley_alive.gif')
        self._btn_dead = tk.PhotoImage(file='resources/smiley_dead.gif')
        self._btn_win = tk.PhotoImage(file='resources/smiley_win.gif')
        self._btn_scared = tk.PhotoImage(file='resources/smiley_scared.gif')

        # Initialize UI
        self.toolbar = ttk.Frame(self, height=32)
        self.smiley_btn = ttk.Button(
            self.toolbar,
            command=lambda: self.prepare_game(10, 12, 15),
        )
        self.smiley_btn.pack()
        self.toolbar.pack(fill=tk.X)

        # Create a game frame that will hold all Mines
        self.game_frame = tk.Frame(self)
        # 2D List of all created Mines objects
        self.buttons = None

        self.prepare_game(10, 12, 15)

    def prepare_game(self, width, height, mines):
        """Generates a new map and initializes UI"""

        # Set smiley face to alive
        self.smiley_btn.config(image=self._btn_alive)

        self.buttons = [[None for c in range(width)] for r in range(height)]

        # Destroy all existing Mines (from previous games)
        for c in self.game_frame.winfo_children():
            c.destroy()

        # Generate a map using generate_minefield() and create a corresponding
        # Mine object for each field
        for r, row in enumerate(generate_minefield(width, height, mines)):
            for c, val in enumerate(row):
                btn = Mine(self.game_frame, value=val)
                # Change smiley face to scared when the left mouse button is pressed
                btn.bind('<Button-1>', lambda e: self.smiley_btn.config(image=self._btn_scared))
                # Call handle_click when the left mouse button is released
                btn.bind('<ButtonRelease-1>', lambda event, r=r, c=c: self.handle_click(r, c))
                # Put the Mine into the grid
                btn.grid(row=r, column=c)
                # Save a reference to the button
                self.buttons[r][c] = btn

        self.game_frame.pack()

    def kaboom(self):
        """Reveals the whole map after clicking on a bomb"""
        for row in self.buttons:
            for mine in row:
                mine.visible = True

    def reveal_bfs(self, row, col):
        """Uses BFS to reveal empty areas starting from the specified coordinates"""

        # List of directions that need to be checked
        directions = [(1, 0), (-1, 0), (0, 1), (0, -1), (1, 1), (1, -1), (-1, 1), (-1, -1)]

        q = deque()
        q.append((row, col))

        while len(q) > 0:
            r, c = q.popleft()
            # Skip if already visible
            if self.buttons[r][c].visible:
                continue

            # Make the button visible
            self.buttons[r][c].visible = True

            # Do not search from the mine if its value is not 0
            if self.buttons[r][c].value != 0:
                continue

            # Search in all 8 directions
            for a, b in directions:
                rr, cc = r+a, c+b

                # Check for boundaries
                if rr < 0 or cc < 0 or rr >= len(self.buttons) or cc >= len(self.buttons[0]):
                    continue
                # Check if not already visible
                if self.buttons[rr][cc].visible:
                    continue

                q.append((rr, cc))

    def handle_click(self, row, col):
        """Handles click on specified Mine, performs action according to its value"""
        mine = self.buttons[row][col]

        if mine.value == -1:
            # Set the smiley to dead and detonate the whole thing
            self.smiley_btn.config(image=self._btn_dead)
            self.kaboom()
            return
        elif mine.value == 0:
            # Use BFS to reveal empty fields
            self.reveal_bfs(row, col)
        else:
            # Reveal the clicked mine
            mine.visible = True

        # Set smiley face back to alive
        self.smiley_btn.config(image=self._btn_alive)
        # Check for winning conditions
        self.check_win()

    def _check_win(self):
        """Helper function for checking the winning conditions

        Checks if all fields that can be safely revealed are revealed
        """
        for row in self.buttons:
            for mine in row:
                if mine.value >= 0 and not mine.visible:
                    return False
        return True

    def check_win(self):
        """Checks for win and displays a message box on win"""
        if self._check_win():
            self.smiley_btn.config(image=self._btn_win)
            messagebox.showinfo('Win', 'Win')


if __name__ == "__main__":
    app = Minesweeper()
    app.title('Minesweeper')
    app.focus_force()

    # Use alternative ttk style
    ttk.Style().theme_use('alt')

    app.mainloop()
