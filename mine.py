from random import randint


def print_minefield(minefield):
    for r in minefield:
        for c in r:
            print(c if c != -1 else '*', end=' ')
        print()


def generate_minefield(width=5, height=7, mines=5):
    minefield = [[0 for c in range(width+2)] for r in range(height+2)]

    # Generate mines
    gen_mines = 0
    while gen_mines < mines:
        r, c = randint(1, height), randint(1, width)

        if minefield[r][c] != -1:
            minefield[r][c] = -1
            gen_mines += 1

    # Calculate values

    for r in range(1, height+1):
        for c in range(1, width+1):
            if minefield[r][c] == -1:
                continue

            total = 0
            for rr in range(r-1, r+2):
                total += minefield[rr][c-1:c+2].count(-1)

            minefield[r][c] = total

    return [row[1:-1] for row in minefield[1:-1]]


if __name__ == "__main__":
    minefield = generate_minefield()
    print_minefield(minefield)
